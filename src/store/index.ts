import { Context, createContext } from "react";

export { reducer } from './reducers';
export const QueryContext: Context<any> = createContext('provider');

QueryContext.displayName = 'provider';