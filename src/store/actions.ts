import { SET_CONDITION, SET_NOT, ADD_GROUP, ADD_RULE, REMOVE_RULE_GROUP, REMOVE_RULE, SET_FIELD, SET_OPERATOR, SET_VALUE, RESET_JSON_GROUP } from './action-types';
import { Action, JsonPathGroup, JsonPathRule, JsonPathRuleGroup, ValueType, Config } from '../types';

export const setCondition = (type: "AND" | "OR", path: string): Action => ({ type: SET_CONDITION, data: {type, path} });

export const setNot = (flag: boolean, path: string): Action => ({ type: SET_NOT, data: {flag, path} });

export const addGroup = (group: JsonPathRuleGroup, path: string): Action => ({ type: ADD_GROUP, data: {group, path} });

export const addRule = (rule: JsonPathRule, path: string): Action => ({ type: ADD_RULE, data: {rule, path} });

export const removeRuleGroup = (path: string): Action => ({ type: REMOVE_RULE_GROUP, data: { path } });

export const removeRule = (path: string): Action => ({ type: REMOVE_RULE, data: { path } });

export const setField = (field: string, path: string): Action => ({ type: SET_FIELD, data: { field, path } });

export const setOperator = (operator: string, config: Config, path: string): Action => ({ type: SET_OPERATOR, data: { operator, config, path } });

export const setValue = (value: ValueType,  path: string): Action => ({ type: SET_VALUE, data: { value, path } });

export const resetJsonGroup = (jsonGroup: JsonPathGroup): Action => ({ type: RESET_JSON_GROUP, data: { jsonGroup } })
