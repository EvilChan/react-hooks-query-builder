/**
 * 设置与或门 && ||
 */
export const SET_CONDITION = "SET_CONDITION";
/**
 * 设置取反
 */
export const SET_NOT = "SET_NOT";
/**
 * 添加组
 */
export const ADD_GROUP = "ADD_GROUP";
/**
 * 添加规则
 */
export const ADD_RULE = "ADD_RULE";
/**
 * 删除规则
 */
export const REMOVE_RULE = "REMOVE_RULE";
/**
 * 删除组
 */
export const REMOVE_RULE_GROUP = "REMOVE_RULE_GROUP";
/**
 * 设置字段名
 */
export const SET_FIELD = "SET_FIELD";
/**
 * 设置运算符
 */
export const SET_OPERATOR = "SET_OPERATOR";
/**
 * 设置值
 */
export const SET_VALUE = "SET_VALUE";
/**
 * 重置
 */
export const RESET_JSON_GROUP = "RESET_JSON_GROUP";
