import { Dispatch } from 'react';

type FieldType = "number" | "string" | "date" | "time" | "datetime" | "select" | "multiselect" | "boolean";

export interface JsonGroup {
  condition: "AND" | "OR",
  rules?: (JsonRule | JsonRuleGroup)[],
  not: boolean,
  valid: boolean,
}

export interface JsonRule {
  id?: string,
  field?: string,
  type?: FieldType,
  input?: string,
  operator?: string,
  value?: any,
}

export interface JsonRuleGroup {
  condition: "AND" | "OR",
  rules?: (JsonRule | JsonRuleGroup)[],
  not: boolean,
}

export interface JsonPathGroup extends JsonGroup {
  path?: string,
  rules?: (JsonPathRule | JsonPathRuleGroup)[],
}

export interface JsonPathRule extends JsonRule {
  path?: string
}

export interface JsonPathRuleGroup extends JsonRuleGroup {
  path?: string,
  rules?: (JsonPathRule | JsonPathRuleGroup)[],
}

export interface Config {
  conditions: {
    [id: string]: { label: string, value: string }
  },
  fields?: {
    [field: string]: {
      label: string,
      value: string,
      type: FieldType,
      operators?: {
        type: string,
        input: {
          type: string,
          inputOptions: Record<string, unknown>,
        },
      }[],
    }
  },
  operators: {
    [id: string]: {
      label: string,
      nb_input: number,
      multiple: boolean,
      apply_to: string[]
    }
  },
  plugins: {
    [id: string]: {
      render: (props: { rule: JsonPathRule, config: Config, dispatch: Dispatch<Action> }) => React.ReactElement,
      apply_to: string[]
    }
  },
  settings: {
    size: "large" | "middle" | "small",
    showNot: boolean,
    showCondition: boolean,
    showActions: boolean,
    showAddGroup: boolean,
    showAddRule: boolean,
    showDelGroup: boolean,
    showDelRule: boolean,
    showDrag: boolean,
    fieldSelectPlaceholder: string,
    operatorsSelectPlaceholder: string,
    valueSrcSelectPlaceholder: string,
    inputPlaceholder: string,
    inputSelectPlaceholder: string,
    enumSelectPlaceholder: string,
    addGroupLabel: string,
    addRuleLabel: string,
    delGroupLabel: string,
    delRuleLabel: string,
    readonly: boolean,
  }
}

export type Action = {
  type: 'SET_CONDITION',
  data: {
    type: 'AND' | 'OR',
    path: string
  }
} | {
  type: 'SET_NOT',
  data: {
    flag: boolean,
    path: string
  }
} | {
  type: 'ADD_GROUP',
  data: {
    group: JsonPathRuleGroup,
    path: string
  }
} | {
  type: 'ADD_RULE',
  data: {
    rule: JsonPathRule,
    path: string
  }
} | {
  type: 'REMOVE_RULE_GROUP',
  data: {
    path: string
  }
} | {
  type: 'REMOVE_RULE',
  data: {
    path: string
  }
} | {
  type: 'SET_FIELD',
  data: {
    field: string,
    path: string
  }
} | {
  type: 'SET_OPERATOR',
  data: {
    operator: string,
    config: Config,
    path: string
  }
} | {
  type: 'SET_VALUE',
  data: {
    value: any,
    path: string,
  }
} | {
  type: 'RESET_JSON_GROUP',
  data: {
    jsonGroup: JsonPathGroup
  }
}
