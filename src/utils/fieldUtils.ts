
const fieldType = {
  number: "number",
  string: "text",
  date: "date",
  datetime: "datetime",
  select: "select",
  boolean: "switch",
}

/**
 * 根据数据类型及运算符得出输入组件名称
 * @param type 数据类型
 * @param operator 运算符
 * @returns 输入组件名称
 */
export const getFieldInputByTypeAndOperator = (type: string, operator: string) => {
  if (typeof type === "undefined" || typeof operator === "undefined") {
    console.error(`type is ${typeof type} or operator is ${typeof operator}!`);
    return void 0;
  }
  if (!fieldType[(type as keyof typeof fieldType)]) {
    console.warn('type not support!');
    return void 0;
  }
  if (operator === "is_null" || operator === "is_not_null" || operator === "is_empty" || operator === "is_not_empty") {
    return void 0;
  }
  return fieldType[(type as keyof typeof fieldType)];
}
