import { Config } from '../types';
import { renderTextInput, renderNumberInput, renderDate, renderDateTime, renderSelect, renderSwitch, renderSlider } from '../components/widgets';

export const defaultConfig: Config = {
  conditions: {
    AND: { label: 'AND', value: 'AND' },
    OR: { label: 'OR', value: 'OR' }
  },
  operators: {
    equal:                { label: '==',            nb_input: 1, multiple: false, apply_to: ['string', 'number', 'datetime', 'date', 'boolean'] },
    not_equal:            { label: '!=',            nb_input: 1, multiple: false, apply_to: ['string', 'number', 'datetime', 'date', 'boolean'] },
    less:                 { label: '<',             nb_input: 1, multiple: false, apply_to: ['number', 'datetime', 'date'] },
    less_or_equal:        { label: '<=',            nb_input: 1, multiple: false, apply_to: ['number', 'datetime', 'date'] },
    greater:              { label: '>',             nb_input: 1, multiple: false, apply_to: ['number', 'datetime', 'date'] },
    greater_or_equal:     { label: '>=',            nb_input: 1, multiple: false, apply_to: ['number', 'datetime', 'date'] },
    in:                   { label: 'in',            nb_input: 1, multiple: true,  apply_to: ['string', 'number'] },
    not_in:               { label: 'not_in',        nb_input: 1, multiple: true,  apply_to: ['string', 'number'] },
    between:              { label: 'between',       nb_input: 2, multiple: false, apply_to: ['number', 'datetime', 'date'] },
    not_between:          { label: 'not_between',   nb_input: 2, multiple: false, apply_to: ['number', 'datetime', 'date'] },
    is_empty:             { label: 'is_empty',      nb_input: 0, multiple: false, apply_to: ['string'] },
    is_not_empty:         { label: 'is_not_empty',  nb_input: 0, multiple: false, apply_to: ['string'] },
    is_null:              { label: 'is_null',       nb_input: 0, multiple: false, apply_to: ['string', 'number', 'datetime', 'date', 'boolean'] },
    is_not_null:          { label: 'is_not_null',   nb_input: 0, multiple: false, apply_to: ['string', 'number', 'datetime', 'date', 'boolean'] },
  },
  plugins: {
    text: {
      render: (props) => renderTextInput(props),
      apply_to: ["string"],
    },
    number: {
      render: (props) => renderNumberInput(props),
      apply_to: ["number"],
    },
    date: {
      render: (props) => renderDate(props),
      apply_to: ["date"],
    },
    datetime: {
      render: (props) => renderDateTime(props),
      apply_to: ["datetime"],
    },
    switch: {
      render: (props) => renderSwitch(props),
      apply_to: ["boolean"],
    },
    select: {
      render: (props) => renderSelect(props),
      apply_to: ["string", "number", "date", "datetime"],
    },
    slider: {
      render: (props) => renderSlider(props),
      apply_to: ["number"],
    }
  },
  settings: {
    showCondition: true,
    showNot: true,
    showActions: true,
    showAddGroup: true,
    showAddRule: true,
    showDelGroup: true,
    showDelRule: true,
    showDrag: false,
    size: "small",
    fieldSelectPlaceholder: '请选择',
    operatorsSelectPlaceholder: '请选择',
    valueSrcSelectPlaceholder: '请选择',
    inputPlaceholder: '请输入',
    inputSelectPlaceholder: '请选择',
    enumSelectPlaceholder: '请选择',
    addGroupLabel: 'addGroup',
    addRuleLabel: 'addRule',
    delGroupLabel: '',
    delRuleLabel: '',
    readonly: false,
  }
}
