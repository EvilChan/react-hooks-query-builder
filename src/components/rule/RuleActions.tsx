import React from 'react';
import { Button } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import { isEqual } from 'lodash';

import { Action, Config } from '../../types';
import { removeRule } from '../../store/actions';

const RuleActions: React.FC<{
  config: Config,
  path: string,
  onClick: (action: Action) => void,
  style?: React.CSSProperties,
}> = React.memo((props) => {
  const { config, path, onClick, style } = props;
  return (
    <Button
      size={config.settings.size}
      style={style}
      icon={<DeleteOutlined />}
      type="default"
      onClick={() => onClick(removeRule(path))}
    >
      {config.settings.delRuleLabel}
    </Button>
  );
}, (prevProps, nextProps) => {
  return isEqual(nextProps.config, prevProps.config);
});

export default RuleActions;
