import React, { useContext, Dispatch } from 'react';

import { QueryContext } from '../../store';
import { JsonPathRule, Config, JsonGroup, Action } from '../../types';

const RuleInput: React.FC<{
    rule: JsonPathRule,
    config: Config
}> = (props) => {
  const { rule, config } = props;
  const { dispatch } = useContext<{ state: JsonGroup; dispatch: Dispatch<Action> }>(QueryContext);

  let inputType = rule.input;
  if (typeof inputType === "undefined") return null;

  const ruleProps = {
    rule, config, dispatch
  };

  return config.plugins[inputType].render(ruleProps);
}

export default RuleInput;
