import React from 'react';
import { SwapOutlined } from '@ant-design/icons';

const RuleDrag: React.FC<{}> = React.memo((props) => {
  return (
    <SwapOutlined rotate={90} style={{marginRight: '5px'}} />
  );
});

export default RuleDrag;
