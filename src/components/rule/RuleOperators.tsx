import React from 'react';
import { Select } from 'antd';
import { OptionData } from 'rc-select/lib/interface';
import { isEqual } from 'lodash';

import { Action, Config } from '../../types';
import { setOperator } from '../../store/actions';

const RuleOperators: React.FC<{
  className?: string,
  config: Config,
  options: OptionData[],
  operator?: string,
  path: string,
  onChange: (action: Action) => void,
}> = React.memo((props) => {
  const { className, config, options, operator, path, onChange } = props;
  return (
    <Select
      className={className}
      size={config.settings.size}
      placeholder={config.settings.operatorsSelectPlaceholder}
      showArrow={!config.settings.readonly}
      style={{pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
      options={options}
      value={operator}
      onChange={(value) => onChange(setOperator(value, config, path))}
    />
  );
}, (prevProps, nextProps) => {
  if (isEqual(nextProps.config, prevProps.config)) {
    if (!isEqual(nextProps.options, prevProps.options)) {
      return false;
    }
    if (!isEqual(nextProps.operator, prevProps.operator)) {
      return false;
    }
    return true;
  } else {
    return false;
  }
});

export default RuleOperators;
