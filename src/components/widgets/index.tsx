import React, { Dispatch } from 'react';

import { JsonPathRule, Config, Action } from '../../types';
import TextInput from './components/TextInput';
import NumberInput from './components/NumberInput';
import MulitNumberInput from './components/MulitNumberInput';
import DateInput from './components/DateInput';
import MulitDateInput from './components/MulitDateInput';
import DateTimeInput from './components/DateTimeInput';
import MulitDateTimeInput from './components/MultiDateTimeInput';
import SwitchInput from './components/SwitchInput';
import SelectInput from './components/SelectInput';
import SliderInput from './components/SliderInput';
import MultiSliderInput from './components/MultiSliderInput';

// Input
export const renderTextInput = (props: { config: Config, rule: JsonPathRule, dispatch: Dispatch<Action> }) => {
  const { config, rule, dispatch } = props;

  const configFieldOperators = config.fields![rule.field as string].operators;
  let inputOptions = {}
  if (configFieldOperators && configFieldOperators.length > 0) {
    const configFieldOperator = configFieldOperators.filter((val) => val.type == rule.operator)[0];
    if (configFieldOperator) {
      inputOptions = configFieldOperator.input.inputOptions;
    }
  }

  return (
    <TextInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  );
}

export const renderNumberInput = (props: { config: Config, rule: JsonPathRule, dispatch: Dispatch<Action> }) => {
  const { config, rule, dispatch } = props;
  const inputLength = config.operators[rule.operator as string].nb_input;

  const configFieldOperators = config.fields![rule.field as string].operators;
  let inputOptions = {}
  if (configFieldOperators && configFieldOperators.length > 0) {
    const configFieldOperator = configFieldOperators.filter((val) => val.type == rule.operator)[0];
    if (configFieldOperator) {
      inputOptions = configFieldOperator.input.inputOptions;
    }
  }

  return inputLength == 1 ? (
    <NumberInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path||""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  ) : (
    <MulitNumberInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path||""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  );
}

export const renderDate = (props: { config: Config, rule: JsonPathRule, dispatch: Dispatch<Action> }) => {
  const { config, rule, dispatch } = props;
  const inputLength = config.operators[rule.operator as string].nb_input;

  const configFieldOperators = config.fields![rule.field as string].operators;
  let inputOptions = {}
  if (configFieldOperators && configFieldOperators.length > 0) {
    const configFieldOperator = configFieldOperators.filter((val) => val.type == rule.operator)[0];
    if (configFieldOperator) {
      inputOptions = configFieldOperator.input.inputOptions;
    }
  }

  return inputLength == 1 ? (
    <DateInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  ) : (
    <MulitDateInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  );
}

export const renderDateTime = (props: { config: Config, rule: JsonPathRule, dispatch: Dispatch<Action> }) => {
  const { config, rule, dispatch } = props;
  const inputLength = config.operators[rule.operator as string].nb_input;

  const configFieldOperators = config.fields![rule.field as string].operators;
  let inputOptions = {}
  if (configFieldOperators && configFieldOperators.length > 0) {
    const configFieldOperator = configFieldOperators.filter((val) => val.type == rule.operator)[0];
    if (configFieldOperator) {
      inputOptions = configFieldOperator.input.inputOptions;
    }
  }

  return inputLength == 1 ? (
    <DateTimeInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  ) : (
    <MulitDateTimeInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  );
}

export const renderSwitch = (props: { config: Config, rule: JsonPathRule, dispatch: Dispatch<Action> }) => {
  const { config, rule, dispatch } = props;

  const configFieldOperators = config.fields![rule.field as string].operators;
  let inputOptions = {}
  if (configFieldOperators && configFieldOperators.length > 0) {
    const configFieldOperator = configFieldOperators.filter((val) => val.type == rule.operator)[0];
    if (configFieldOperator) {
      inputOptions = configFieldOperator.input.inputOptions;
    }
  }

  return (
    <SwitchInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  );
}

export const renderSelect = (props: { config: Config, rule: JsonPathRule, dispatch: Dispatch<Action> }) => {
  const { config, rule, dispatch } = props;

  const configFieldOperators = config.fields![rule.field as string].operators;
  let inputOptions = {}
  if (configFieldOperators && configFieldOperators.length > 0) {
    const configFieldOperator = configFieldOperators.filter((val) => val.type == rule.operator)[0];
    if (configFieldOperator) {
      inputOptions = configFieldOperator.input.inputOptions;
    }
  }

  return (
    <SelectInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  );
}

export const renderSlider = (props: { rule: JsonPathRule, config: Config, dispatch: Dispatch<Action> }) => {
  const { config, rule, dispatch } = props;
  const inputLength = config.operators[rule.operator as string].nb_input;

  const configFieldOperators = config.fields![rule.field as string].operators;
  let inputOptions = {}
  if (configFieldOperators && configFieldOperators.length > 0) {
    const configFieldOperator = configFieldOperators.filter((val) => val.type == rule.operator)[0];
    if (configFieldOperator) {
      inputOptions = configFieldOperator.input.inputOptions;
    }
  }

  return inputLength == 2 ? (
    <MultiSliderInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  ) : (
    <SliderInput
      config={config}
      inputOptions={inputOptions}
      path={rule.path || ""}
      value={rule.value}
      onChange={(action) => dispatch(action)}
    />
  );
}
