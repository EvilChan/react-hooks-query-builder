import React from 'react';
import { Switch } from 'antd';
import { isEqual } from 'lodash';

import { Action, Config } from '../../../types';
import { setValue } from '../../../store/actions';

const SwitchInput: React.FC<{
  config: Config,
  inputOptions: any,
  path: string,
  value?: boolean,
  onChange: (action: Action) => void,
}> = React.memo((props) => {
  const { config, inputOptions, path, value, onChange } = props;
  return (
    <Switch
      style={{width: '50px', pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
      size="default"
      checked={value}
      onChange={(e) => onChange(setValue(e, path))}
      {...inputOptions}
    />
  );
}, (prevProps, nextProps) => {
  if (isEqual(nextProps.config, prevProps.config)) {
    return isEqual(nextProps.value, prevProps.value);
  } else {
    return false;
  }
});

export default SwitchInput;
