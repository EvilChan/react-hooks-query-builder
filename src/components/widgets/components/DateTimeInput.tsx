import React from 'react';
import { DatePicker } from 'antd';
import zh_CN from 'antd/lib/locale/zh_CN';
import moment from 'moment';
import { isEqual } from 'lodash';

import { Config, Action } from '../../../types';
import { setValue } from '../../../store/actions';

const DateTimeInput: React.FC<{
  config: Config,
  inputOptions: any,
  path: string,
  onChange: (action: Action) => void,
  value?: any,
}> = React.memo((props) => {
  const { config, path, value, onChange, inputOptions } = props;
  return (
    <DatePicker
      style={{width: '150px', pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
      locale={zh_CN.DatePicker}
      size={config.settings.size}
      showTime
      value={value ? moment(value) : void 0}
      onChange={(e) => e && onChange(setValue(e.format('YYYY-MM-DD'), path))}
      {...inputOptions}
    />
  );
}, (prevProps, nextProps) => {
  if (isEqual(nextProps.config, prevProps.config)) {
    return isEqual(nextProps.value, prevProps.value);
  } else {
    return false;
  }
});

export default DateTimeInput;
