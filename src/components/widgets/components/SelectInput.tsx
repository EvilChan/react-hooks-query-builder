import React from 'react';
import { Select } from 'antd';
import { isEqual } from 'lodash';

import { Action, Config } from '../../../types';
import { setValue } from '../../../store/actions';

const SelectInput: React.FC<{
  config: Config,
  inputOptions: any,
  path: string,
  value?: any,
  onChange: (action: Action) => void,
}> = React.memo((props) => {
  const { config, inputOptions, path, value, onChange } = props;
  return (
    <Select
      size={config.settings.size}
      style={{width: '120px', pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
      placeholder={config.settings.inputSelectPlaceholder}
      value={value}
      onChange={(e) => onChange(setValue(e, path))}
      {...inputOptions}
    />
  );
}, (prevProps, nextProps) => {
  if (isEqual(nextProps.config, prevProps.config)) {
    return isEqual(nextProps.value, prevProps.value);
  } else {
    return false;
  }
});

export default SelectInput;
