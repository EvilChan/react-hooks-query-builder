import React from 'react';
import { Slider } from 'antd';
import { isEqual } from 'lodash';

import { Action, Config } from '../../../types';
import { setValue } from '../../../store/actions';

const SliderInput: React.FC<{
  config: Config,
  inputOptions: any,
  path: string,
  value?: any,
  onChange: (action: Action) => void,
}> = React.memo((props) => {
  const { config, inputOptions, path, value, onChange } = props;
  return (
    <Slider
      style={{width: '200px', pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
      min={1}
      max={100}
      value={value}
      onChange={(e) => onChange(setValue(e, path))}
      {...inputOptions}
    />
  );
}, (prevProps, nextProps) => {
  if (isEqual(nextProps.config, prevProps.config)) {
    return isEqual(nextProps.value, prevProps.value);
  } else {
    return false;
  }
});

export default SliderInput;
