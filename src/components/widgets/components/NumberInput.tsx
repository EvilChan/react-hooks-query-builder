import React from 'react';
import { InputNumber } from 'antd';
import { isEqual } from 'lodash';

import { Action, Config } from '../../../types';
import { setValue } from '../../../store/actions';

const NumberInput: React.FC<{
  config: Config,
  inputOptions: any,
  path: string,
  value?: number,
  onChange: (action: Action) => void,
}> = React.memo((props) => {
  const { config, inputOptions, path, value, onChange } = props;
  return (
    <InputNumber
      style={{width: '120px', pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
      size={config.settings.size}
      placeholder={config.settings.inputPlaceholder}
      value={value}
      onChange={(e) => onChange(setValue(e, path))}
      {...inputOptions}
    />
  );
}, (prevProps, nextProps) => {
  if (isEqual(nextProps.config, prevProps.config)) {
    return isEqual(nextProps.value, prevProps.value);
  } else {
    return false;
  }
});

export default NumberInput;
