import React from 'react';
import { Input } from 'antd';
import { isEqual } from 'lodash';

import { Config, Action } from '../../../types';
import { setValue } from '../../../store/actions';

const TextInput: React.FC<{
  config: Config,
  inputOptions: any,
  path: string,
  onChange: (action: Action) => void,
  value?: string | number | string[],
}> = React.memo((props) => {
  const { config, path, value, onChange, inputOptions } = props;
  return (
    <Input
      style={{width: '120px', pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
      size={config.settings.size}
      placeholder={config.settings.inputPlaceholder}
      value={value}
      onChange={(e) => onChange(setValue(e.target.value, path))}
      {...inputOptions}
    />
  );
}, (prevProps, nextProps) => {
  if (isEqual(nextProps.config, prevProps.config)) {
    return isEqual(nextProps.value, prevProps.value);
  } else {
    return false;
  }
});

export default TextInput;
