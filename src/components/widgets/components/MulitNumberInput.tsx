import React from 'react';
import { Input, InputNumber } from 'antd';
import { isEqual } from 'lodash';

import { Action, Config } from '../../../types';
import { setValue } from '../../../store/actions';

const MulitNumberInput: React.FC<{
  config: Config,
  inputOptions: any,
  path: string,
  value?: number[],
  onChange: (action: Action) => void,
}> = React.memo((props) => {
  const { config, inputOptions, path, value, onChange } = props;
  return (
    <Input.Group>
      <InputNumber
        style={{width: '120px', pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
        size={config.settings.size}
        placeholder={config.settings.inputPlaceholder}
        value={value ? value[0] : void 0}
        onChange={(e) => onChange(setValue(value ? [e, value[1]]: [e], path))}
        {...inputOptions}
      />
      <InputNumber
        style={{width: '120px', pointerEvents: config.settings.readonly ? 'none' : 'auto'}}
        size={config.settings.size}
        placeholder={config.settings.inputPlaceholder}
        value={value ? value[1] : void 0}
        onChange={(e) => onChange(setValue(value ? [value[0], e]: [void 0, e], path))}
        {...inputOptions}
      />
    </Input.Group>
  );
}, (prevProps, nextProps) => {
  if (isEqual(nextProps.config, prevProps.config)) {
    return isEqual(nextProps.value, prevProps.value);
  } else {
    return false;
  }
});

export default MulitNumberInput;
