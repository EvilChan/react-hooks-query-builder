import React, { useState, useEffect } from 'react';
import { Row, Col, Input, Select, Checkbox, DatePicker } from 'antd';

import Query from '../components/QueryBuilder';
import { JsonGroup, JsonPathGroup, JsonPathRule, JsonPathRuleGroup, Config } from '../types';
import { defaultConfig } from '../utils';

/**
 *  1、参考jQueryBuilder并基于系统前端技术架构开发一个前端插件，该插件与业务无关，定位为纯表达式的设计器：
 *  2、插件支持外部参数传入，并控制编辑器可使用的功能：
 *      1）是否可添加分组；
 *      2）是否可添加条件；
 *      3）可使用的运算符；
 *  3、插件基础功能：
 *      1）、可按分组设置过滤条件；规则/分组间支持And、Or、NOT运算符连接；
 *      2）、编辑表达式时，操作项数据类型有对应的运算符，不同运算符有对应的输入组件，输入组件受字段类型及运算符影响。
 *      * 支持的运算符（对应SQL运算符）
 *          * equal ---------------------- (==)
 *          * not_equal ---------------- (!=)
 *          * less --------------------------(<)
 *          * less_or_equal ----------- (<=)
 *          * greater -------------------- (>)
 *          * greater_or_equal ------ (>=)
 *          * like -------------------------- (LIKE)
 *          * not_like -------------------- (NOT LIKE)
 *          * between ------------------ (BETWEEN * AND *)
 *          * not_between ------------ (NOT BETWEEN * AND *)
 *          * is_empty ------------------ (IS EMPTY)
 *          * is_not_empty ------------ (IS NOT EMPTY)
 *          * select_any_in ------------ (IN)
 *          * select_not_any_in ------ (NOT IN)
 *      3）、根据数据类型对应的输入组件
 *          * CHAR、VACHAR -- 文本框 (text)
 *          * INT、BIGINT -- 数值文本框、滑动条 (number)
 *          * DATE -- 日期组件 (date)
 *          * TIME -- 时间组件 (time)
 *          * DATETIME、TIMESTAMP -- 日期时间组件 (datetime)
 *          * 不限类型 -- 下拉组件 (select)
 *          * 不限类型 -- 多选下拉组件 (multiselect)
 *          * switch开关组件 (boolean)
 *      4）、个别类型或个别运算符有针对的组件处理
 *          * 布尔类型对应的组件是switch开关
 *          * 数值类型对应的组件有数值输入框及滑动组件
 *          * 数值类型的between运算符对应的是双向滑动组件
 *      5）、支持删除规则、分组；
 */


const SqlBuilder: React.FC<{
  config?: Config,
  value?: JsonGroup,
  onChange?: (data: any) => void
}> = (props) => {
  const [data, setData] = useState<JsonGroup>();
  const [config, setConfig] = useState<Config>({
    ...defaultConfig,
    settings: {
      ...defaultConfig.settings,
      addGroupLabel: '添加组',
      addRuleLabel: '添加规则'
    }
  });

  useEffect(() => {
    console.log('QueryBuilder = ', data);
  }, [data])

  useEffect(() => {
    const timer = setTimeout(() => {
      setConfig({
        ...config,
        fields: {
          // 文本基础
          name: {
            label: 'name',
            value: 'name',
            type: 'string',
          },
          // 自定义
          name2: {
            label: 'name2',
            value: 'name2',
            type: 'string',
          },
          // 枚举
          name3: {
            label: 'name3',
            value: 'name3',
            type: 'string',
          },
          // 枚举超过6个，组件变更
          name4: {
            label: 'name4',
            value: 'name4',
            type: 'string',
          },
          // 数值基本
          age: {
            label: 'age',
            type: 'number',
            value: 'age',
          },
          // 带滑动条的数值组件
          age2: {
            label: 'age2',
            type: 'number',
            value: 'age2',
          },
          // 默认自定义带滑动条的数值组件
          age3: {
            label: 'age3',
            type: 'number',
            value: 'age3',
          },
          datetime: {
            label: 'datetime',
            type: 'datetime',
            value: 'datetime',
          },
          rangeAge: {
            label: 'rangeAge',
            type: 'number',
            value: 'rangeAge',
          },
          gender: {
            label: 'gender',
            type: 'boolean',
            value: 'gender'
          }
        }
      });
    }, 3000);
    return () => {
      clearTimeout(timer);
    }
  }, [])

  const renderQueryBuilderParser = () => {
    let newState = '';

    function _processNode(item: JsonGroup | JsonPathGroup | JsonPathRuleGroup | undefined) {
      if (!item) {
        return
      }
      const rules = item.rules;
      if (rules) {
        for (let id in rules) {
          const currentRule = (rules[id] as unknown) as JsonPathRule;
          if (currentRule.value) {
            newState += `${currentRule.field} ${config.operators[currentRule.operator as string].label} ${currentRule.value}`;
            if ((id as unknown as number) < rules.length - 1) {
              newState += ` ${item.condition} `;
            }
          }
          _processNode(rules[id] as JsonPathRuleGroup);
        }
      }
    }

    _processNode(data);

    return newState;
  }

  return (
    <div style={{ width: '800px'}}>
      <Query config={config} value={data} onChange={(data) => setData(data)} />
      {renderQueryBuilderParser()}
    </div>
  );
};

export default SqlBuilder;


// {
//   condition: "OR",
//   rules: [
//     {
//       id: "name",
//       field: "name",
//       type: "number",
//       input: "text",
//       operator: "equal",
//       value: void 0,
//     },
//     {
//       id: "age",
//       field: "age",
//       type: "boolean",
//       input: "text",
//       operator: "equal",
//       value: void 0,
//     },
//     {
//       id: "datetime",
//       field: "datetime",
//       type: "datetime",
//       input: "datetime",
//       operator: "equal",
//       value: void 0,
//     },
//     {
//       id: "datetime",
//       field: "datetime",
//       type: "datetime",
//       input: "datetime",
//       operator: "between",
//       value: void 0,
//     },
//     {
//       id: "rangeAge",
//       field: "rangeAge",
//       type: "number",
//       input: "number",
//       operator: "between",
//       value: void 0,
//     },
//     {
//       id: "gender",
//       field: "gender",
//       type: "boolean",
//       input: "switch",
//       operator: "equal",
//       value: void 0,
//     }
//   ],
//   not: false,
//   valid: true
// }
